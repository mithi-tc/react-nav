/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useRef,useState,useEffect} from 'react';
import { ActivityIndicator,TouchableOpacity,StyleSheet,Button, View,Text,FlatList } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { WebView } from 'react-native-webview';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TextInput } from 'react-native-gesture-handler';

// function RootScreen({ navigation }) {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Root</Text>
//     </View>
//   );
// }

function About({ navigation }) {
  return (
    <View style={{ alignItems: 'center', justifyContent: 'center',margin:20,backgroundColor:'white', }}>
      <Text style={{fontSize:20,color:'black',padding:20}}>
        React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.
   {"\n\n"}
Use a little—or a lot. You can use React Native today in your existing Android and iOS projects or you can create a whole new app from scratch.</Text>
    </View>
  );
}

const storeData = async (value,navigation) => {
  try {
    await AsyncStorage.setItem('@storage_Key', value)
    // console.log(value)
    navigation.navigate('Settings')
  } catch (e) {
    // saving error
  }
}

function Profile({ navigation }) {

const [name,setName] = useState(null)
  
const getData = async () => {
  try {
    const value = await AsyncStorage.getItem('@storage_Key')
    // console.log("updating")
    // console.log(value)
    if(value !== null) {
      // value previously stored
      setName(value);
    }
  } catch(e) {
    // error reading value
  }
}

useEffect(()=>{
  getData()});

 return (
      <View style={{ margin:20 }}>
        <Text style={{fontSize:18}}> <Text >Name</Text>: {name ? name: <Text style={styles.emptyName}> Not set </Text> }</Text>
      </View>
    )

}

function HomeScreen({ navigation }) {
  return (
    <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Weather') {
              iconName = focused
                ? 'weather-cloudy'
                : 'weather-cloudy-alert';
            } else if (route.name === 'Google') {
              iconName = focused ? 'search-web' : 'text-search';
            }
            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'blue',
          inactiveTintColor: 'gray',
        }}
      >
    <Tab.Screen name="Weather" component={WeatherScreen} />
    <Tab.Screen name="Google" component={GoogleScreen} />
  </Tab.Navigator>
  );
}

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'About',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Profile',
  },
];

const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);


function FlatListElements ({navigation}) {
  
  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate(item.title)}>
      <Item title={item.title} />
      </TouchableOpacity>
  );

  return (
    <FlatList
    data={DATA}
    renderItem={renderItem}
    keyExtractor={item => item.id}
  />
);
}

function Edit ({navigation}) {
  const [name,setName] = useState(null)

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_Key')
      // console.log(value)
      if(value !== null) {
        // value previously stored
        setName(value);
      }
    } catch(e) {
      // error reading value
    }
  }

  useEffect(()=>{
    getData()},[]
    );
  

  return(
    <View style={{ alignItems: 'center', justifyContent: 'center',margin:20 }}>
      <Text style={styles.miniSaveHeading}> Set Name</Text>
      <TextInput onChangeText={(name)=>setName(name)} defaultValue={name} style={styles.inputBox} />
      <TouchableOpacity onPress={() => storeData(name,navigation)}>
      <Text style={styles.saveButton}>Save</Text>
      </TouchableOpacity>
    </View>
  );
}


function SettingsScreen({ navigation }) {

return (  
<Stack.Navigator>
    <Stack.Screen name="Settings" component={FlatListElements} />
    <Stack.Screen name="About" component={About}  />
    <Stack.Screen name="Profile" component={Profile}  options={{ headerRight: () => (
      <TouchableOpacity   onPress={() => navigation.navigate('Edit')} >
        <Text style={styles.editButton}> Edit </Text>
      </TouchableOpacity>)
    }} />
     <Stack.Screen name="Edit" component={Edit} />
  </Stack.Navigator>
);
}

function WeatherScreen() {

  const [data,setData] = useState(null);
  const [loading,setLoading] = useState(false);
  const [didMount, setDidMount] = useState(false); 

useEffect(() => {
   setDidMount(true);
   return () => setDidMount(false);
}, [])

if(!didMount) {
  return null;
}

  const getWeatherFromApiAsync = async () => {
    
    try {
      let response = await fetch(
        'http://api.openweathermap.org/data/2.5/weather?q=London&appid=2ca0aad18afe1d85d679b037a7ca2be8'
      );
      let json = await response.json();
      setData(json.weather[0].description);
      setLoading(false);
      // console.log(data);
      // console.log(loading);
    } catch (error) {
      console.error(error);
      setLoading(false);

    }
  }
  
  return (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',padding:40}}>
  <Button
  onPress={() => {getWeatherFromApiAsync(); setLoading(true)}}
  title="Check Weather"
  />
    { loading ? <ActivityIndicator/> : 
    <View>
      {data?
         <Text style={styles.weatherText}> {data} </Text>
         :
         <Text></Text>
      }
      </View>
      } 
 </View>
  );
}

function GoogleScreen() {

  const webview = useRef(null)
  return (
    <WebView
    ref={webview}
    startInLoadingState
    style={{ flex: 1 }}
    source={{ uri: "https://google.com" }}
  />
  );
}


const Drawer = createDrawerNavigator();

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();



const App: () => React$Node = () => {
  return (
    <>
      <NavigationContainer>
      {/* <Drawer.Navigator initialRouteName="Root"> */}
      <Drawer.Navigator initialRouteName="Home">
        {/* <Drawer.Screen name="Root" component={RootScreen} /> */}
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Settings" component={SettingsScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({ 
  item: {  
      padding: 20,  
      fontSize: 18,  
      height: 70,  
  }, 
  title:{
    color:'#24a0ed',
    fontWeight:'bold',
    fontSize:20,
    backgroundColor:'white',
    padding:10,
  },
  weatherText: {
    padding:20,
    textTransform:"uppercase",
    fontSize:15,
    backgroundColor:'white',
    color:'black',
    margin:40,
  },
  saveButton: {
    backgroundColor:'skyblue',
    padding:10,
    margin:10,
    width:75,
    color:'white',
    fontSize:20},

  miniSaveHeading:{
    fontSize:20,
    margin:10,
    padding:10,
  },
  inputBox: {
  backgroundColor:'white',
  width:350,
  marginBottom:20,
  borderStyle:"solid",
  borderWidth:1
},
  editButton:{
   fontSize:20,
   fontWeight:"bold",
  },
  emptyName:{
    fontWeight:"bold",
  }
})  

export default App;
